# Fonction rm

rm_fct() {
    read -p "Quel fichier voulez vous supprimer : " fichier_supprimer

    if [ -f "$fichier_supprimer" ]; then
        rm "$fichier_supprimer"
        echo "Fichier '$fichier_supprimer' supprimé avec succès."
    else
        echo "Le fichier '$fichier_supprimer' n'existe pas ou n'est pas accessible."
    fi
}