#!/usr/bin/bash

source help.sh
source passw.sh
source profil.sh
source quit.sh
source open.sh
source about.sh
source hour.sh
source age.sh
source pwd.sh
source ls.sh
source cd.sh
source rm.sh
source rmd_rmdir.sh
source version.sh
source httpget.sh
source cmd_inconnues.sh


cmd() {
  cmd=$1
  argv=$*

  case "${cmd}" in
    connexion ) connexion;;
    quit | exit | q ) quit;;
    help ) help;;
    about ) about;;  # Appel de la fonction "about"
    age ) age;;  # Appel de la fonction "age"
    hour ) hour;;  # Appel de la fonction "hour"
    passw ) passw;;  
    profil ) profil;;
    pwd ) pwd_fct;;
    ls ) ls_fct $1 $2;;
    cd ) cd_fct $2;;
    rm ) rm_fct;;
    rmd | rmdir ) rmd_rmdir_fct;;
    open ) open_fct;;
    version | --v | vers ) version_fct;;
    httpget ) httpget_fct;;
    * ) cmd_inconnue;;
  esac
}

main() {
  lineCount=1

    echo "Entrez votre login: "
    read login

    echo "Entrez votre mdp: "
    read -s mdp

  while [ 1 ]; do
    date=$(date +%H:%M)
    echo -ne "${date} - [\033[34m${lineCount}\033[m] - \033[34ma_omr\033[m ~ 😄 ~ "
    read string

    cmd $string
    lineCount=$(($lineCount+1))
  done
}

main
