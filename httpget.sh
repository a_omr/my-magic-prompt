# Fonction httpget

httpget_fct() {
    
    read -p "URL de la page web que vous souhaitez télécharger : " url
    read -p "Nom du fichier : " fichier_destination

    curl -o "$fichier_destination" "$url" # Télécharger la page web et enregistrer le contenu dans le fichier grâce à curl

    if [ $? -eq 0 ]; then
        echo "Page web téléchargée avec succès dans '$fichier_destination'."
    else
        echo "Erreur lors du téléchargement de la page web."
    fi
}