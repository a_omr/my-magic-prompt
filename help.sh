# Fonction help 

function help() {
    echo "Les instructions disponible:"
    echo "help : Affiche les commandes disponibles"
    echo "ls : Lister des fichiers et les dossiers visible comme caché"
    echo "rm :  Supprimer un fichier"
    echo "rmd ou rmdir : Supprimer un dossier"
    echo "about : Une description de votre programme"
    echo "version ou --v ou vers : Affiche la version de votre prompt"
    echo "age :  Vous demande votre âge et vous dit si vous êtes majeur ou mineur"
    echo "quit : Permet de sortir du prompt"
    echo "profil : Permet d’afficher toutes les informations sur vous même.
                -First Name, Last name, age, email"
    echo "passw : Permet de changer le password avec une demande de confirmation"
    echo "cd : Aller dans un dossier que vous venez de créer ou de revenir à un dossier précédent"
    echo "pwd : Indique le répertoire actuelle courant"
    echo "hour : Permet de donner l’heure actuelle"
    echo "* : Indiquer une commande inconnu"
    echo "httpget : Permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le nom du fichier."
    echo "smtp :  Vous permet d’envoyer un mail avec une adresse un sujet et le corp du mail"
    echo "open : Ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas

"
}
