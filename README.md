<<<<<<< README.md
# My Magic Prompt Documentation

## Introduction
This custom prompt offers a set of commands to handle various tasks. You need to authenticate with a specific login and password to access the prompt. All commands should be executed within the `~/my-magic-prompt/main.sh` directory.

## Available Commands

### help
The `help` command displays the list of available commands and their descriptions.

### ls
The `ls` command allows you to list visible files and folders, including hidden files.

### rm
The `rm` command allows you to delete a file.

### rmd or rmdir
The `rmd` or `rmdir` command allows you to delete a directory.

### about
The `about` command displays a description of the program.

### version
The `version` command displays the prompt's version.

### age
The `age` command asks for your age and tells you whether you are a minor or an adult.

### quit
The `quit` command allows you to exit the prompt.

### profil
The `profil` command displays all information about yourself, such as your first name, last name, age, and email.

### passw
The `passw` command allows you to change the password with a confirmation prompt.

### cd
The `cd` command allows you to navigate between folders, including creating a folder or returning to the previous folder.

### pwd
The `pwd` command indicates the current working directory.

### hour
The `hour` command displays the current time.

### *
The `*` command indicates an unknown command.

### httpget
The `httpget` command allows you to download the HTML source code of a web page and save it in a specific file by requesting a file name.

### smtp
The `smtp` command allows you to send an email with an address, subject, and email body.

### open
The `open` command allows you to open a file directly in the VIM editor, even if the file does not exist.


