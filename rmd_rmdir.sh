
# Fonction rmd ou rmdir

rmd_rmdir_fct() {
    read -p "Quel dossier voulez vous supprimer : " dossier_supprimer

    if [ -d "$dossier_supprimer" ]; then
        rmdir "$dossier_supprimer"
        echo "Dossier '$dossier_supprimer' supprimé avec succès."
    elif [ -e "$dossier_supprimer" ]; then
        # Utilisation de rm -r pour supprimer un dossier et son contenu
        rm -r "$dossier_supprimer"
        echo "Dossier '$dossier_supprimer' et son contenu ont été supprimés avec succès."
    else
        echo "Le dossier '$dossier_supprimer' n'existe pas ou n'est pas accessible."
    fi
}