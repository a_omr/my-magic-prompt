# Fonction version

version_fct() {
    git_version=$(git describe --tags)

    if [ -n "$git_version" ]; then
        echo "Version du prompt : $git_version"
    else
        echo "La version du prompt n'a pas été trouvée dans Git."
    fi
}
