# Fonction about

about() {
    
    echo "Bienvenue dans My Magic Prompt!"
    echo "Ce prompt a été créé pour vous fournir un ensemble de commandes utiles."
    echo "Vous pourrez retrouver la liste des commandes disponibles avec la commande <help>."

}